import React from "react";

import { ReactWidget } from "@jupyterlab/apputils";
import { ContentsManager } from "@jupyterlab/services";

/**
 * React component for a counter.
 *
 * @returns The React component
 */
const IframeComponent = (): JSX.Element => {
  const [room, setRoom] = React.useState("");

  const getProjectName = async (): Promise<string> => {
    let proj = "unknow";
    try {
      const contents = new ContentsManager();
      const modelConfig = await contents.get("japps_env");
      const content = JSON.parse(modelConfig.content);
      console.log(content);
      proj = content.JAPPS_PROJECT;
    } catch {
      console.warn("no configuration file, revert to default");
    }
    return proj;
  };

  React.useEffect(() => {
    getProjectName().then((project) => {
      setRoom(`${project}3246fccb,Xx-mnOphQDw31JEQYVfXpQ`);
    });
  }, []);

  return (
    <div>
      <iframe
        className="jp-excalidraw-Widget"
        src={`https://excalidraw.com/#room=${room}`}
      />
    </div>
  );
};

/**
 * A Counter Lumino Widget that wraps a IframeComponent.
 */
export class IframeWidget extends ReactWidget {
  /**
   * Constructs a new IframeWidget.
   */

  render(): JSX.Element {
    return <IframeComponent />;
  }
}
