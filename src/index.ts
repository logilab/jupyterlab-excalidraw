import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
} from "@jupyterlab/application";
import { MainAreaWidget } from "@jupyterlab/apputils";
import { ILauncher } from "@jupyterlab/launcher";
import { LabIcon } from "@jupyterlab/ui-components";

import { IframeWidget } from "./widget";
import excalidrawIconStr from "../style/excalidraw.svg";

/**
 * The command IDs used by the react-widget plugin.
 */
namespace CommandIDs {
  export const create = "excalidraw";
}

/**
 * Initialization data for the react-widget extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: "excalidraw",
  autoStart: true,
  optional: [ILauncher],
  activate: (app: JupyterFrontEnd, launcher: ILauncher) => {
    const { commands } = app;
    const icon = new LabIcon({
      name: "launcher:excalidraw",
      svgstr: excalidrawIconStr,
    });
    const command = CommandIDs.create;
    commands.addCommand(command, {
      caption: "Open excalidraw",
      label: "Excalidraw",
      icon: (args) => (args["isPalette"] ? null : icon),
      execute: () => {
        const content = new IframeWidget();
        const widget = new MainAreaWidget<IframeWidget>({ content });
        widget.title.label = "Excalidraw";
        widget.title.icon = icon;
        app.shell.add(widget, "main");
      },
    });

    if (launcher) {
      launcher.add({
        command,
        category: "Apps",
      });
    }
  },
};

export default extension;
